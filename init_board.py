from pieces.originals import *
from pieces.news import *
from pieces.knightlike import *
from pieces.doubles import *
from pieces.cannons import *
from pieces.combos import *
from pieces.piece import Mage

board = [
    [
        Lance, Lion, Jack, Ace, Chariot, Knightmare, Amazon, Empress,
        King, Amazon, Knightmare, Chariot, Ace, Jack, Lion, Lance
    ],

    [Chancellor, Dragon, Chameleon, Elephant, Trebuchet, Leo, Queen, HookMover],
    [Rook, Ghoul, Imitator, Kangaroo, Giraffe, General, Vao, Camel],
    [Pao, Gryphon, Zebra, Camel, Cobra, Knight, Mage, Bishop],
    [Pawn]*16
] + [[None]*16]*3