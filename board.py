from pieces.originals import *
from pieces.piece import Promotable
from init_board import board as starter

class Board:
    def __init__(self, board=None):
        self.side = -1
        self.previous = None
        if board is not None:
            self.board = board
        else:
            self.board = self.init_board()

    @property
    def width(self):
        return len(self.board[0])

    @property
    def height(self):
        return len(self.board)

    def init_board(self):
        width = len(starter[0])
        s = [row+[*reversed(row)] if len(row)*2 == width else row for row in starter]
        if len(s) < width:
            s = s+[*reversed(s)]

        return [
            [
                None if t is None else t(self, (i, j), -1 if i > len(s)//2 else 1)
                for j, t in enumerate(row)
            ] for i, row in enumerate(s)
        ]


    def __getitem__(self, item):
        if item[0] < 0 or item[1] < 0:
            raise IndexError('index out of range')

        return self.board[item[0]][item[1]]

    def __setitem__(self, key, value):
        self.board[key[0]][key[1]] = value

    def __str__(self):
        s = ''
        for row in self.board:
            for piece in row:
                if piece is not None:
                    s += f'{str(piece): >3} '
                else:
                    s += ' -- '

            s += '\n'

        return s

    def __copy__(self):
        new = self.__class__()
        board = [
            [
                None if piece is None else piece.copy(new) for piece in row
            ]for row in self.board
        ]
        new.board = board
        new.previous = self.previous
        new.side = self.side
        return new

    def move(self, start, end, check=True):
        if self[start] is None:
            raise ValueError('No piece')

        if self[start].side != self.side:
            raise ValueError('Wrong side')

        if isinstance(self.previous, tuple) and start != self.previous:
            raise ValueError('Cannot move that piece')

        piece = self[start]
        if end not in piece.get_all_valid_moves():
            raise ValueError('Invalid destination')

        if check:
            tester = self.__copy__()
            tester.move(start, end, check=False)
            if tester.in_check(self[start].side):
                raise ValueError('Move puts you in check')

        status = piece.make_move(end)
        self.previous = piece.__class__
        if status == 10 and isinstance(piece, Promotable):
            piece.promote()
            return status

        self.side *= -1
        # Any en passant is over now, chances will be created next move
        self.clear_ep(self.side)
        if check:
            mate = not self.can_move(self.side)
            if mate:
                if self.in_check(self.side):
                    return self.side * -1, 'checkmate'
                else:
                    return self.side * -1, 'stalemate'

    def clear_ep(self, side):
        for row in self.board:
            for piece in row:
                if isinstance(piece, Pawn) and piece.side == side:
                    piece.en_passant = []

    def in_check(self, side):
        for row in self.board:
            for piece in row:
                if isinstance(piece, King) and piece.side == side:
                    return piece.in_check()

        raise ValueError('King not found')

    def can_move(self, side):
        for i, row in enumerate(self.board):
            for j, piece in enumerate(row):
                if piece is None or piece.side != side: continue
                for move in piece.get_all_valid_moves():
                    tester = self.__copy__()
                    tester.move(piece.position, move, check=False)
                    if not tester.in_check(self.side):
                        return True

        return False
    """
    def get_valid_moves(self, position):
        return self[position].get_all_valid_moves()
    """


if __name__ == '__main__':
    board = Board()
    def p(): print(board, end='\n'*3)

    p()
    while True:
        x = input().split()
        x = list(map(int, x))
        try:
            status = board.move((x[0], x[1]), (x[2], x[3]))
        except ValueError as e:
            print(e)
        else:
            p()
            if status is not None and isinstance(status, tuple) and status[0] in (1, -1):
                print(status)
                break
