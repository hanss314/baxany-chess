class ArgumentCountException(ValueError):
    def __init__(self, expected: int):
        self.expected = expected