from .piece import StandardCapAndMove
from .originals import Bishop, Queen


class DoubleMoverWithoutCaptures(StandardCapAndMove):
    def __init__(self, base, *args):
        super().__init__(*args)
        self.base = base

    def get_valid_moves(self):
        moves = set()
        caps = set()
        for di in self.base.get_directions(self):
            d = 1
            while True:
                pos = di[0] * d, di[1] * d
                try: p = self.get_at_delta(pos)
                except IndexError:  break

                if p is None: moves.add(pos)
                elif p.side == self.side:  break
                else:
                    caps.add(pos)
                    break

                d += 1

        seconds = set()
        for move in set(moves):
            tester = self.base(self.board, self.delta_to_abs(move), self.side)
            seconds |= tester.get_valid_moves()

        moves |= caps
        moves = map(self.delta_to_abs, moves)
        return set(moves) | seconds


class HookMover(DoubleMoverWithoutCaptures):
    symbol = 'H'

    def __init__(self, *args):
        super().__init__(Bishop, *args)


class Empress(DoubleMoverWithoutCaptures):
    symbol = 'Em'

    def __init__(self, *args):
        super().__init__(Queen, *args)


class Joker(DoubleMoverWithoutCaptures):
    symbol = 'J'

    def __init__(self, *args):
        from .news import Knightmare
        super().__init__(Knightmare, *args)