from abc import ABC, abstractmethod
from errors import *


class Piece(ABC):
    symbol = ''

    def __init__(self, board, position, side):
        self.board = board
        self.position = position
        # side should be -1 or 1
        self.side = side
        super().__init__()

    def next_to_mage(self):
        for x in range(-1, 2):
            for y in range(-1, 2):
                if x == y == 0: continue
                try: p = self.get_at_delta((y, x))
                except IndexError: continue
                if isinstance(p, Mage):
                    return True

        return False

    def get_all_valid_moves(self):
        if self.next_to_mage():
            mage = Mage(self.board, self.position, self.side)
            return mage.get_valid_moves() | self.get_valid_moves()

        return self.get_valid_moves()

    def make_move(self, position):
        mage = Mage(self.board, self.position, self.side)
        if self.next_to_mage() and position in mage.get_valid_moves():
            if self.board[position] is not None:
                self.board[position].capture(self)
                self.capturing(self.board[position])

            self.board[position] = self
            self.position = position

        annotations = self.move.__annotations__
        if 'position' in annotations and len(position) != len(annotations['position']):
            raise ArgumentCountException(len(annotations['position']))

        return self.move(position)

    @abstractmethod
    def get_valid_moves(self):
        pass

    @abstractmethod
    def move(self, position):
        pass

    @abstractmethod
    def capture(self, piece):
        """Called when captured"""
        pass

    @abstractmethod
    def capturing(self, piece):
        """Called when capturing"""

    def __str__(self):
        s = self.symbol
        if self.side == -1:
            s = '*' + s

        return s

    def copy(self, board):
        return self.__class__(board, self.position, self.side)

    def remove_friendlies(self, moves):
        return {
            m for m in moves
            if self.board[m] is None or self.board[m].side != self.side
        }

    def remove_offboard(self, moves):
        return {
            m for m in moves
            if 0 <= m[0] < self.board.height and 0 <= m[1] < self.board.width
        }

    def delta_to_abs(self, delta):
        y, x = self.position
        return y + delta[0], x + delta[1]

    def get_at_delta(self, delta):
        return self.board[self.delta_to_abs(delta)]

class Promotable(Piece):
    @abstractmethod
    def promote(self):
        pass

    def at_end(self):
        return (self.side == 1 and self.position == self.board.height - 1) or \
               (self.side == -1 and self.position == 0)

    def convert(self, piece):
        self.board[self.position] = piece(self.board, self.position, self.side)


class StandardCapAndMove(Piece):
    def move(self, position):
        """
        if position not in self.get_all_valid_moves():
            raise ValueError('Invalid destination')
            """

        if self.board[position] is not None:
            piece = self.board[position]
            piece.capture(self)
            self.capturing(piece)

        self.board[position] = self
        self.board[self.position] = None
        self.position = position

    def capture(self, piece):
        pass

    def capturing(self, piece):
        pass


class StraightLineWithStops(StandardCapAndMove):
    @abstractmethod
    def get_directions(self) -> list:
        pass

    def get_valid_moves(self):
        moves = set()
        for di in self.get_directions():
            d = 1
            while True:
                pos = di[0]*d, di[1]*d
                try: p = self.get_at_delta(pos)
                except IndexError: break

                if p is None or p.side != self.side: moves.add(pos)
                if p is not None: break

                d += 1

        moves = map(self.delta_to_abs, moves)
        moves = self.remove_offboard(moves)
        return set(moves)


class KnightLike(StandardCapAndMove):
    direction = (1, 2)

    def get_valid_moves(self):
        moves = [self.direction, (-self.direction[0], self.direction[1])]
        moves += [(-y, -x) for y, x in moves]
        moves += [(x, y) for y, x in moves]
        moves = map(self.delta_to_abs, moves)
        moves = self.remove_offboard(moves)
        moves = self.remove_friendlies(moves)
        return set(moves)


class Mage(KnightLike):
    symbol = 'M'
