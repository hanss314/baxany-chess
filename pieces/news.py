from .originals import *
from .piece import StraightLineWithStops


class Lance(StraightLineWithStops, Promotable):
    symbol = 'La'

    def get_directions(self):
        return [(self.side, 0)]

    def move(self, position):
        super().move(position)
        if self.at_end(): return 10

    def promote(self):
        super().convert(Rook)


class Knightmare(StraightLineWithStops):
    symbol = 'Nm'

    def get_directions(self):
        return [
            (2, 1), (2, -1), (-2, 1), (-2, -1),
            (1, 2), (1, -2), (-1, 2), (-1, -2)
        ]

class General(StandardCapAndMove):
    symbol = 'G'

    def get_valid_moves(self):
        moves = King.get_valid_moves(self)
        for y in range(-2, 3):
            for x in range(-2, 3):
                try: p = self.get_at_delta((y, x))
                except IndexError: continue
                if isinstance(p, King) and p.side == self.side:
                    moves.add(p.position)

        return moves

    def move(self, position):
        p = self.board[position]
        if isinstance(p, King) and p.side == self.side:
            self.board[self.position] = p
            self.board[position] = self
            p.position = self.position
            self.position = position
        else:
            super().move(position)


class Imitator(Queen):
    symbol = 'I'

    def get_valid_moves(self):
        moves = set()
        for di in Queen.get_directions(self):
            d = 1
            while True:
                pos = di[0] * d, di[1] * d
                try: p = self.get_at_delta(pos)
                except IndexError: break

                if p is None: moves.add(pos)
                elif p.side != self.side and not isinstance(p, Imitator) and self.position in p.get_all_valid_moves():
                    moves.add(pos)
                    break

                d += 1

        moves = map(self.delta_to_abs, moves)
        moves = self.remove_offboard(moves)
        return set(moves)


class Lion(StandardCapAndMove):
    symbol = 'Li'

    def __init__(self, *args):
        super().__init__(*args)
        self.captured = False

    def get_valid_moves(self):
        jumps = {(y, x, -1, -1) for y in range(-2, 3) for x in range(-2, 3) if not (x == y == 0)}
        jumps = set(map(self.delta_to_abs, jumps))
        jumps = self.remove_friendlies(self.remove_offboard(jumps))

        self.board[self.position] = None
        doubles = {
            (*first, *second)
            for first in King(self.board, self.position, self.side).get_valid_moves()
            for second in King(self.board, first, self.side).get_valid_moves()
        }

        self.board[self.position] = self

        return jumps | doubles

    def move(self, position: (int,)*4):
        super().move(position[:2])
        if self.captured: return
        if -1 not in position[2:]:
            super().move(position[2:])

    def capture(self, piece):
        self.captured = True


class Dragon(Lion, Rook):
    symbol = 'D'

    def __init__(self, *args):
        Lion.__init__(self, *args)

    def get_valid_moves(self):
        moves = Lion.get_valid_moves(self)
        moves |= Rook.get_valid_moves(self)

        return moves

    def move(self, position: (int,)*4):
        if position[2] == position[3] == -1 and position[:2] in Rook.get_valid_moves(self):
            Rook.move(self, position)
        else:
            Lion.move(self, position)


class Chameleon(StandardCapAndMove):
    symbol = 'Cm'

    def get_valid_moves(self):
        if not issubclass(self.board.previous, Piece): return set()
        emulating = self.board.previous(self.board, self.position, self.side)
        return emulating.get_all_valid_moves()

    def move(self, position):
        emulating = self.board.previous(self.board, self.position, self.side)
        self.board[self.position] = emulating
        status = emulating.move(position)
        self.board[emulating.position] = self
        self.position = emulating.position
        return status


class Chariot(Queen):
    symbol = 'Cr'

    def get_valid_moves(self):
        queen_moves = Queen.get_valid_moves(self)
        queen_moves = {(*move, -1, -1) for move in queen_moves}
        chariot_moves = set()
        for direction in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
            try: piece = self.get_at_delta(direction)
            except IndexError: continue
            if piece.side == self.side:
                for di in self.get_directions():
                    d = 1
                    while True:
                        pos = di[0]*d, di[1]*d
                        try: cp = piece.get_at_delta(pos)
                        except IndexError: break

                        try: mp = self.get_at_delta(pos)
                        except IndexError: break

                        if mp is not None: break
                        if cp is None or cp.side != self.side:
                            chariot_moves.add((*direction, *self.delta_to_abs(pos)))

                        if cp is not None: break

                        d += 1

        return queen_moves | chariot_moves


    def move(self, position: (int,)*4):
        if position[2] == position[3] == -1:
            Queen.move(self, position[:2])
        else:
            rider = self.get_at_delta(position[:2])
            Queen.move(self, position[2:])
            rider_pos = self.get_at_delta(position[:2])
            self.board[rider.position] = None
            if self.board[rider_pos] is not None:
                self.board[rider_pos].capture(self)

            self.board[rider_pos] = rider
            rider.position = rider_pos

class Elephant(StandardCapAndMove):
    symbol = 'El'

    def get_valid_moves(self):
        return self.remove_offboard(
            map(
                self.delta_to_abs,
                Queen.get_directions(self)
            )
        )

    def move(self, position):
        direction = position[0] - self.position[0], position[1] - self.position[1]
        piece = self
        self.board[self.position] = None
        while True:
            new_position = piece.delta_to_abs(direction)
            try:
                p = self.board[new_position]
            except IndexError:
                piece.capture(self)
                break

            self.board[new_position] = piece
            piece.position = new_position

            if p is None: break
            else: piece = p


class Ghoul(StandardCapAndMove):
    symbol = 'Gh'

    def get_valid_moves(self):
        return {
            (y, x)
            for y in range(self.board.height)
            for x in range(self.board.width)
            if self.board[y, x] is None
        }

    def capture(self, piece):
        self.board[piece.position] = None
        piece.capture(self)


class Ace(StandardCapAndMove, Promotable):
    symbol = 'Ac'

    def __init__(self, *args):
        super().__init__(*args)
        self.captures = 0

    def copy(self, board):
        c = super().copy(board)
        c.captures = self.captures
        return c

    def get_valid_moves(self):
        return King.get_valid_moves(self)

    def capturing(self, piece):
        from .doubles import Empress
        if isinstance(piece, Empress):
            self.promote()
        elif not isinstance(piece, Pawn):
            self.captures += 1
            if self.captures >= 10:
                self.promote()

    def promote(self):
        from .doubles import Joker
        super().convert(Joker)


class Jack(StandardCapAndMove):
    symbol = 'Ja'

    def get_valid_moves(self):
        moves = map(self.delta_to_abs, Queen.get_directions(self))
        moves = {(y, x%self.board.width) for y, x in moves}
        moves = self.remove_friendlies(self.remove_offboard(moves))
        return moves


class Gryphon(StandardCapAndMove):
    symbol = 'Gy'

    def get_valid_moves(self):
        moves = {
            (y, x, -1, -1)
            for y in range(-2, 3)
            for x in range(-2, 3)
            if not y == x == 0
        }

        moves = self.remove_friendlies(self.remove_offboard(moves))
        pushes = set()
        for direction in Queen.get_directions(self):
            di = direction[0]*3, direction[1]*3
            try: piece = self.get_at_delta(di)
            except IndexError: continue

            if piece is not None and piece.side != self.side:
                for d in range(1, 4):
                    delta = direction[0]*d, direction[1]*d
                    try:
                        if piece.get_at_delta(delta) is not None:
                            break
                    except IndexError:
                        break

                    pushes.add((*self.delta_to_abs(di), piece.delta_to_abs(delta)))


        return moves | pushes

    def move(self, position: (int,)*4):
        if position[2] == position[3] == -1:
            return super().move(position[:2])

        piece = self.board[position[:2]]
        self.board[position[:2]] = None
        self.board[position[2:]] = piece
        piece.position = position[2:]


class Trebuchet(Elephant):
    symbol = 'Tr'

    def get_valid_moves(self):
        elephants = super().get_valid_moves()
        elephants = {(*p, -1, -1) for p in elephants}
        flings = set()
        for delta in Rook.get_directions(self):
            try: piece = self.get_at_delta(delta)
            except IndexError: continue
            start = self.delta_to_abs(delta)
            for i in range(1, 6):
                i *= self.side
                end = piece.delta_to_abs((i, 0))
                if 0 <= end[0] < self.board.height:
                    flings.add((*start, *end))
                else:
                    break

        return elephants | flings

    def move(self, position):
        if position[2] == position[3] == -1:
            return super().move(position[:2])

        piece = self.board[position[:2]]
        return StandardCapAndMove.move(piece, position[2:])
