from .piece import KnightLike

class Cobra(KnightLike):
    direction = (2, 2)
    symbol = 'C'


class Camel(KnightLike):
    direction = (3, 1)
    symbol = 'Ca'


class Zebra(KnightLike):
    direction = (3, 2)
    symbol = 'Z'


class Giraffe(KnightLike):
    direction = (4, 2)
    symbol = 'Gi'


class Kangaroo(KnightLike):
    direction = (4, 3)
    symbol = 'Kg'