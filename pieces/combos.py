from .piece import StandardCapAndMove, Piece
from .originals import Queen, Bishop, Rook, Knight


class Combo(StandardCapAndMove):
    def __init__(self, a, b, *args):
        super().__init__(*args)
        self.a, self.b = a, b

    def get_valid_moves(self):
        return self.a.get_valid_moves(self) | self.b.get_valid_moves(self)

class Amazon(Combo, Queen, Knight):
    symbol = 'A'

    def __init__(self, *args):
        super().__init__(Queen, Knight, *args)


class Chancellor(Combo, Rook, Knight):
    symbol = 'Ch'

    def __init__(self, *args):
        super().__init__(Rook, Knight, *args)


class Cardinal(Combo, Bishop, Knight):
    symbol = 'Ca'

    def __init__(self, *args):
        super().__init__(Bishop, Knight, *args)