from .piece import StandardCapAndMove, StraightLineWithStops, Promotable, Piece
from .knightlike import KnightLike


class Pawn(StandardCapAndMove, Promotable):
    symbol = 'P'
    def __init__(self, *args):
        self.moved = False
        super().__init__(*args)
        self.en_passant = []

    def copy(self, board):
        c = super().copy(board)
        c.moved = self.moved
        c.en_passant = self.en_passant
        return c

    def move(self, position):
        old = self.position
        super().move(position)
        if self.moved == False:
            self.en_passant = [(i, old[1]) for i in range(*sorted((old[0] + self.side, position[0])))]

        self.moved = True
        for i in range(self.board.height):
            p = self.board[i, self.position[1]]
            if self.can_en_passant(p, position):
                p.capture(self)

        if self.at_end(): return 10

    def can_en_passant(self, piece, position):
        return piece is not None and \
               isinstance(piece, Pawn) and \
               piece.side != self.side and \
               position in piece.en_passant

    def capture(self, _):
        self.board[self.position] = None

    def promote(self):
        super().convert(Queen)

    def get_valid_moves(self):
        moves = set()
        for i in range(1, 2 if self.moved else 4):
            pos = (self.side * i, 0)
            try:
                piece = self.get_at_delta(pos)
            except IndexError:
                break
            else:
                if piece is None:
                    moves.add(pos)
                else:
                    break

        for i in (-1, 1):
            try:
                p = self.delta_to_abs((self.side, i))
                piece = self.get_at_delta((self.side, i))
            except IndexError: pass
            else:
                if piece is not None and piece.side != self.side:
                    moves.add((self.side, i))

                for h in range(self.board.height):
                    if self.can_en_passant(self.board[h, self.position[1] + i], p):
                        moves.add((self.side, i))


        moves = map(self.delta_to_abs, moves)
        moves = self.remove_offboard(moves)
        moves = self.remove_friendlies(moves)
        return set(moves)


class Rook(StraightLineWithStops):
    symbol = 'R'

    def get_directions(self):
        return [(0, 1), (0, -1), (1, 0), (-1, 0)]


class Bishop(StraightLineWithStops):
    symbol = 'B'

    def get_directions(self):
        return [(1, 1), (1, -1), (-1, 1), (-1, -1)]


class Queen(StraightLineWithStops):
    symbol = 'Q'

    def get_directions(self):
        return [
            (0, 1), (0, -1), (1, 0), (-1, 0),
            (1, 1), (1, -1), (-1, 1), (-1, -1)
        ]


class Knight(KnightLike):
    symbol = 'N'
    direction = (1, 2)


class King(StandardCapAndMove):
    symbol = 'K'

    def get_valid_moves(self):
        moves = [(y, x) for y in range(-1, 2) for x in range(-1, 2) if x != 0 or y != 0]
        moves = map(self.delta_to_abs, moves)
        moves = self.remove_offboard(moves)
        moves = self.remove_friendlies(moves)
        return set(moves)

    def in_check(self):
        for y in range(self.board.height):
            for x in range(self.board.width):
                p = self.board[y, x]
                if p is not None and p.side != self.side:
                    for move in p.get_all_valid_moves():
                        tester = self.board.__copy__()
                        tester[p.position].make_move(move)
                        new = tester[self.position]
                        if not isinstance(new, King) or (isinstance(new, King) and new.side != self.side):
                            return True

        return False


