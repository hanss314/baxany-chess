from .piece import StraightLineWithStops, Piece
from .originals import Rook, Bishop, Queen


class JumpCapture(StraightLineWithStops):
    def get_valid_moves(self):
        moves = set()
        for di in self.get_directions():
            d = 1
            cancap = False
            while True:
                pos = di[0] * d, di[1] * d
                try: p = self.get_at_delta(pos)
                except IndexError: break

                if cancap:
                    if p is not None and p.side != self.side:
                        moves.add(pos)
                else:
                    if p is None:
                        moves.add(pos)
                    else:
                        cancap = True

                d += 1

        moves = map(self.delta_to_abs, moves)
        moves = self.remove_offboard(moves)
        return set(moves)


class Pao(JumpCapture, Rook):
    symbol = 'Pa'


class Vao(JumpCapture, Bishop):
    symbol = 'V'


class Leo(JumpCapture, Queen):
    symbol = 'V'
